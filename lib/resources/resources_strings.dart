
class MyStrings {
  static const String appBarTitle = 'Deine Job';
  static const String appBarTitle2 = 'website';
  static const String buttonTitle = 'Kostenlos Registrieren';
  static const String tabTitle1 = 'Arbeitnehmer';
  static const String tabTitle2 = 'Arbeitgeber';
  static const String tabTitle3 = 'Temporärbüro';
  static const String underTapBarContent = 'Drei einfache Schritte';
  static const String underTapBarContent2 = 'zu deinem neuen Job';
  static const String bannerTitle1 = 'Erstellen dein Lebenslauf';
  static const String bannerTitle2 = 'Mit nur einem Klick bewerben';



}
