class MyAssets {
  static String undraw_agreement = 'undraw_agreement'.svg;
  static String undraw_personal = 'undraw_personal'.svg;
  static String undraw_profile = 'undraw_profile'.svg;
  static String undraw_task = 'undraw_task'.svg;
  static String arrow1 = 'arrow1'.svg;
  static String arrow2 = 'arrow2'.svg;
}

extension Photo on String {
  String get svg => 'assets/svg/$this.svg';
}
