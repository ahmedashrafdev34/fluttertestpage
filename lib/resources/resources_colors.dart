import 'package:flutter/material.dart';

class MyColors {
  // White
  static Color white = HexColor.fromHex('#FFFFFF');
  static Color zomp = HexColor.fromHex('#319795');
  static Color tuftsBlue = HexColor.fromHex('#3182CE');
  static Color mediumSkyBlue = HexColor.fromHex('#81E6D9');
  static Color aliceBlue = HexColor.fromHex('#EBF4FF');
  static Color bubbles = HexColor.fromHex('#E6FFFA');
  static Color slateGray = HexColor.fromHex('#718096');
}

extension HexColor on Color {
  static Color fromHex(String hexColorString) {
    hexColorString = hexColorString.replaceAll('#', '');
    if (hexColorString.length == 6) {
      hexColorString = 'FF$hexColorString';
    }
    return Color(int.parse(hexColorString, radix: 16));
  }
}
