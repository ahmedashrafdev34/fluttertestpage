import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_test_index/screens/index_imports.dart';


void main() {
  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 640),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (BuildContext context, Widget? child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Test Index',
          home: IndexPage(),
          theme: ThemeData(
            fontFamily: 'Lato'
          ),
          initialRoute: '/',
          routes: {
            '/index' : (context) => IndexPage()
          },
        );
      },
    );
  }
}
