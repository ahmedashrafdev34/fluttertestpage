part of '../index_imports.dart';

class IndexPage extends StatefulWidget {
  @override
  State<IndexPage> createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxWidth < 990) {
            return Container(
              decoration: BoxDecoration(
                color: MyColors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: MyColors.slateGray,
                      blurRadius: 2,
                      offset: Offset(3, 0),
                    )
                  ]

                  ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    alignment: Alignment.center,
                    height: 37.h,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        12.r,
                      ),
                      gradient: LinearGradient(
                        colors: [
                          MyColors.zomp,
                          MyColors.tuftsBlue,
                        ],
                      ),
                    ),
                    child: Text(
                      MyStrings.buttonTitle,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 14.sp,

                        color: MyColors.white,
                        // fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            );
          }
          return SizedBox.shrink();
        },
      ),
      body: Scrollbar(
        child: ListView(
          children: [
            LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
                if (constraints.maxWidth > 990) {
                  return Column(
                    children: [
                      IndexAppBar(),
                      SizedBox(
                        height: 1,
                      ),
                      IndexHeader(),
                      IndexTabBar(),
                    ],
                  );
                } else {
                  return Column(
                    children: [
                      IndexAppBar(),
                      SizedBox(
                        height: 1,
                      ),
                      IndexHeaderMobile(),
                      IndexTabBarMobile(),
                    ],
                  );
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
