import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_screenutil/src/size_extension.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test_index/resources/resources_colors.dart';
import 'package:flutter_test_index/resources/resources_images.dart';
import 'package:flutter_test_index/resources/resources_strings.dart';


part 'index_page/index_page.dart';
part 'widgets_web/index_appbar.dart';
part 'widgets_web/index_header.dart';
part 'widgets_web/index_tab_bar.dart';
part 'widgets_web/TabsView.dart';

part 'widgets_mobile/index_header_mobile.dart';
part 'widgets_mobile/index_tab_bar_mobile.dart';