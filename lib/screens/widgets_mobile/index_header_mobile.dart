part of '../index_imports.dart';

class IndexHeaderMobile extends StatefulWidget {
  @override
  State<IndexHeaderMobile> createState() => _IndexHeaderMobileState();
}

class _IndexHeaderMobileState extends State<IndexHeaderMobile> {
  bool hoverButton = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 20.h,
      ),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          end: Alignment.centerRight,
          begin: Alignment.centerLeft,
          colors: [
            MyColors.aliceBlue,
            MyColors.bubbles,
          ],
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            MyStrings.appBarTitle,
            style: TextStyle(
              fontSize: 55.sp,
              // fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            MyStrings.appBarTitle2,
            style: TextStyle(
              fontSize: 55.sp,
              // fontWeight: FontWeight.bold,
            ),
          ),

          SvgPicture.asset(
            MyAssets.undraw_agreement,
            fit: BoxFit.cover,
            height: 400,
            width: MediaQuery.of(context).size.width,
          ),
        ],
      ),
    );
  }
}
