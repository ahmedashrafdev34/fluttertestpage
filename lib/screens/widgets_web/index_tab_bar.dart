part of '../index_imports.dart';

class IndexTabBar extends StatefulWidget {
  @override
  State<IndexTabBar> createState() => _IndexTabBarState();
}

class _IndexTabBarState extends State<IndexTabBar>
    with SingleTickerProviderStateMixin {
  late final TabController _tabController;

  var currentValue = 0;

  @override
  void initState() {
    _tabController = TabController(vsync: this, length: 3);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(30.0),
          child: Container(
            height: 35.h,
            width: 135.w,
            margin: EdgeInsets.symmetric(horizontal: 100.w),
            child: TabBar(
              labelPadding: EdgeInsets.zero,
              unselectedLabelColor: MyColors.zomp,
              labelColor: MyColors.white,
              overlayColor: MaterialStateProperty.all(
                MyColors.bubbles,
              ),
              controller: _tabController,
              // indicatorSize: TabBarIndicatorSize.tab,
              indicatorColor: Colors.transparent,
              indicatorWeight: 0.1,
              onTap: (value) {
                setState(() {
                  currentValue = value;
                });
              },
              indicator: BoxDecoration(
                color: MyColors.mediumSkyBlue,
                border: Border.all(
                  color: Colors.grey.withOpacity(0.3),
                ),
                borderRadius: currentRadius(_tabController.index),
              ),
              tabs: [
                _createTab(
                  text: MyStrings.tabTitle1,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12.r),
                    bottomLeft: Radius.circular(12.r),
                  ),
                ),
                _createTab(
                  text: MyStrings.tabTitle2,
                  borderRadius: BorderRadius.only(),
                ),
                _createTab(
                  text: MyStrings.tabTitle3,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(12.r),
                    bottomRight: Radius.circular(12.r),
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            children: [
              Text(
                MyStrings.underTapBarContent,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: MyColors.slateGray,
                  fontSize: 25.sp,
                ),
              ),
              Text(
                MyStrings.underTapBarContent2,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: MyColors.slateGray,
                  fontSize: 25.sp,
                ),
              ),
            ],
          ),
        ),
        TabsView(
          tabController: currentValue,
          firstTab: buildStack(),
          secondTab:buildStack(),
          thirdTab: buildStack(),
        ),
      ],
    );
  }

  Stack buildStack() {
    return Stack(
          children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    buildRowTitle(
                      title: MyStrings.bannerTitle1,
                      num: '1',
                    ),
                    SizedBox(
                      width: 40.w,
                    ),
                    SvgPicture.asset(
                      MyAssets.undraw_profile,
                      fit: BoxFit.contain,
                      alignment: Alignment.center,
                      matchTextDirection: true,
                      // allowDrawingOutsideViewBox: true,
                      // excludeFromSemantics: true,
                      width: 100.w,
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 200.h),
                  child: ClipPath(
                    clipper: WaveClipperTwo(
                      // reverse: true,
                      // flip: true,
                    ),
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 15.h,),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            MyColors.bubbles,
                            MyColors.aliceBlue,
                          ]
                        )
                      ),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                MyAssets.undraw_task,
                                fit: BoxFit.contain,
                                alignment: Alignment.center,
                                matchTextDirection: true,
                                // allowDrawingOutsideViewBox: true,
                                // excludeFromSemantics: true,
                                width: 100.w,
                              ),
                              SizedBox(
                                width: 50.w,
                              ),
                              buildRowTitle(
                                title: MyStrings.bannerTitle1,
                                num: '2',
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 150.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      buildRowTitle(
                        title: MyStrings.bannerTitle2,
                        num: '3',
                      ),
                      SizedBox(
                        width: 40.w,
                      ),
                      SvgPicture.asset(
                        MyAssets.undraw_personal,
                        fit: BoxFit.contain,
                        alignment: Alignment.center,
                        matchTextDirection: true,
                        // allowDrawingOutsideViewBox: true,
                        // excludeFromSemantics: true,
                        width: 100.w,
                      ),
                    ],
                  ),
                ),

              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 200.h, left: 70.w),
              child: SvgPicture.asset(
                MyAssets.arrow2,
                // allowDrawingOutsideViewBox: true,
                // excludeFromSemantics: true,
                width: 130.w,
                height: 330.h,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 630.h, left: 70.w),
              child: SvgPicture.asset(
                MyAssets.arrow1,
                // allowDrawingOutsideViewBox: true,
                // excludeFromSemantics: true,
                width: 130.w,
                height: 260.h,
              ),
            ),

          ],
        );
  }

  Row buildRowTitle({
    required String title,
    required String num,
  }) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          num,
          style: TextStyle(
            color: MyColors.slateGray,
            fontWeight: FontWeight.bold,
            fontSize: 115.sp,
          ),
        ),
        SizedBox(width: 5.w,),
        Container(
          margin: EdgeInsets.only(top: 70.h),
          width: 5.w,
          height: 25.h,
          decoration: BoxDecoration(
            color: MyColors.slateGray,
            shape: BoxShape.circle,
          ),
        ),
        SizedBox(
          width: 5.w,
        ),
        Padding(
          padding: EdgeInsets.only(top: 60.h),
          child: Text(
            title,
            style: TextStyle(
              color: MyColors.slateGray,
              fontSize: 28.sp,
            ),
          ),
        ),
      ],
    );
  }

  Widget _createTab({
    required String text,
    required BorderRadiusGeometry borderRadius,
  }) {
    return Tab(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Container(
              child: Center(
                child: Text(
                  text,
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: borderRadius,
                border: Border.all(
                  color: Colors.grey.withOpacity(0.3),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  BorderRadiusGeometry currentRadius(int value) {
    if (value == 0) {
      return BorderRadius.only(
        topLeft: Radius.circular(12.r),
        bottomLeft: Radius.circular(12.r),
      );
    } else if (value == 2) {
      return BorderRadius.only(
        topRight: Radius.circular(12.r),
        bottomRight: Radius.circular(12.r),
      );
    } else {
      return BorderRadius.only();
    }
  }
}
