part of '../index_imports.dart';

class TabsView extends StatelessWidget {
  final Widget firstTab, secondTab, thirdTab;
  final int tabController;

  const TabsView({
    required this.firstTab,
    required this.secondTab,
    required this.thirdTab,
    required this.tabController,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        tabController == 0
            ? AnimatedContainer(
                child: firstTab,
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.symmetric( vertical: 20),
                transform: Matrix4.translationValues(
                  tabController == 0 ? 0 : MediaQuery.of(context).size.width,
                  0,
                  0,
                ),
                duration: Duration(milliseconds: 300),
                curve: Curves.easeIn,
              )
            : Container(),
        tabController == 1
            ? AnimatedContainer(
                child: secondTab,
                width: MediaQuery.of(context).size.width,
                padding:
                    const EdgeInsets.symmetric( vertical: 20),
                transform: Matrix4.translationValues(
                  tabController == 1 ? 0 : MediaQuery.of(context).size.width,
                  0,
                  0,
                ),
                duration: Duration(milliseconds: 300),
                curve: Curves.easeIn,
              )
            : Container(),
        tabController == 2
            ? AnimatedContainer(
          child: thirdTab,
          width: MediaQuery.of(context).size.width,
          padding:
          const EdgeInsets.symmetric( vertical: 20),
          transform: Matrix4.translationValues(
            tabController == 2 ? 0 : MediaQuery.of(context).size.width,
            0,
            0,
          ),
          duration: Duration(milliseconds: 300),
          curve: Curves.easeIn,
        )
            : Container(),
      ],
    );
  }
}
