part of '../index_imports.dart';

class IndexAppBar extends StatefulWidget {
  @override
  State<IndexAppBar> createState() => _IndexAppBarState();
}

class _IndexAppBarState extends State<IndexAppBar> {
  bool showUnderlineText = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: 5.h,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                MyColors.zomp,
                MyColors.tuftsBlue,
              ],
            ),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 67.h,
          decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(20.r),
              bottomLeft: Radius.circular(20.r),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                offset: Offset(0, 3),
                blurRadius: 6,
              )
            ],
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Login',
                        style: TextStyle(
                          color: showUnderlineText
                              ? MyColors.mediumSkyBlue
                              : MyColors.zomp,
                          fontWeight: FontWeight.bold,
                          fontSize: 15.sp,
                        ),
                      ),
                      SizedBox(
                        height: 1,
                      ),
                      Offstage(
                        offstage: !showUnderlineText,
                        child: Container(
                          color: MyColors.mediumSkyBlue,
                          height: 2.h,
                          width: 10.w,
                        ),
                      )
                    ],
                  ),
                  onTap: () {},
                  onHover: (v) {
                    if (v) {
                      setState(() {
                        showUnderlineText = true;
                      });
                    } else {
                      setState(() {
                        showUnderlineText = false;
                      });
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
