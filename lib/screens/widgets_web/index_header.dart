part of '../index_imports.dart';

class IndexHeader extends StatefulWidget {
  @override
  State<IndexHeader> createState() => _IndexHeaderState();
}

class _IndexHeaderState extends State<IndexHeader> {
  bool hoverButton = false;

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: DiagonalPathClipperTwo(),

      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 40.h,
        ),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            end: Alignment.centerRight,
            begin: Alignment.centerLeft,
            colors: [
              MyColors.aliceBlue,
              MyColors.bubbles,
            ],
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  MyStrings.appBarTitle,
                  style: TextStyle(
                    fontSize: 55.sp,
                    // fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  MyStrings.appBarTitle2,
                  style: TextStyle(
                    fontSize: 55.sp,
                    // fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 50.h,
                ),
                InkWell(
                  onTap: () {},
                  onHover: (v) {
                    if (v) {
                      setState(() {
                        hoverButton = true;
                      });
                    } else {
                      setState(() {
                        hoverButton = false;
                      });
                    }
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 37.h,
                    width: 90.w,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        12.r,
                      ),
                      gradient: hoverButton
                          ? LinearGradient(
                              colors: [
                                MyColors.zomp,
                                Colors.black.withOpacity(0.5),
                              ],
                            )
                          : LinearGradient(
                              colors: [
                                MyColors.zomp,
                                MyColors.tuftsBlue,
                              ],
                            ),
                    ),
                    child: Text(
                      MyStrings.buttonTitle,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 14.sp,

                        color: MyColors.white,
                        // fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              width: 20.w,
            ),
            Container(
              decoration: BoxDecoration(
                color: MyColors.white,
                shape: BoxShape.circle,
              ),
              child: SvgPicture.asset(
                MyAssets.undraw_agreement,
                fit: BoxFit.contain,

                alignment: Alignment.center,
                matchTextDirection: true,
                // allowDrawingOutsideViewBox: true,
                // excludeFromSemantics: true,
                width: 130.w,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
